-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 24, 2017 at 09:43 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chatbox`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` text NOT NULL,
  `message` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `message_en` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `timestamp`, `name`, `message`, `message_en`) VALUES
(197, '2017-04-28 07:06:23', 'Shunpei', 'ç§ã‚‚ãƒ“ãƒ¼ãƒ«ã¨ç§˜å¯†ã‚’é£²ã‚€ã®ãŒå¤§å¥½ãï¼', 'I also love drinking beer and secret!!'),
(198, '2017-05-03 09:33:17', 'Misaki', 'ã“ã‚“ã«ã¡ã¯ã€ã‚ãªãŸã¯ã©ã“ã§é£Ÿã¹ã¾ã™ã‹ï¼Ÿ', 'Hello, where are you going to eat?'),
(199, '2017-05-03 09:35:19', 'Aki', 'ç§ã¯ã‚¸ãƒ§ãƒªãƒ™ã§é£Ÿã¹ã‚‹ã¤ã‚‚ã‚Šã§ã™', 'Im going to eat at Jolibee'),
(200, '2017-05-06 04:40:37', 'Wata', 'ç§ã¯ãƒãƒ³ãƒãƒ¼ã‚¬ãƒ¼ã‚­ãƒ³ã‚°ã‚’é£Ÿã¹ãŸã„ã€‚', 'I want to eat burger king.'),
(201, '2017-05-06 08:23:01', 'Ody', 'ç§ã¯ãƒ”ã‚¶ã‚’é£Ÿã¹ãŸã„ï¼', 'I want to eat pizza!'),
(202, '2017-05-06 08:24:35', 'Ody', 'ç§ã¯ãƒ”ã‚¶ã‚’é£Ÿã¹ãŸã„ï¼', 'I want to eat pizza!'),
(204, '2017-08-21 08:38:41', 'Ody', 'ãŠå…ƒæ°—ã§ã™ã‹ï¼Ÿ', 'How are you?');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
